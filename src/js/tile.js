//thought: this data is used to calculate the correct pixel array for each corresponding tile.
//an alternative, more direct approach would be to just save out the corresponding chunk of 
//data from the array and save it with respect to each tile. i dont see the benefit in that
//right now, but depending on the features i want this thing to do it might be worth researching 
//the actual size of each tile in memory and compare which approach is better

class Tile {
    constructor(x, y, source, map, avg, index) {
        this.x = x;
        this.y = y;
        this.source = source;
        this.map = map;
        this.avg = avg;
        this.index = index;
    }
}