//global todos:
//1. decide on an error delivery system. console logging doesnt cut it for common errors, but some errors
//might not need exposed to the user (registry errors, for example)

class Vela {
    constructor(selector, options) {
        if (typeof selector !== 'string' || selector.length <= 0) {
            throw new TypeError('selector must be string of nonzero length');
        }

        this.selector = selector;
        this.defaults = {
            x: 10,
            y: 10,
            master: null,
            samples: [],
            result: new Image(),
            mode: 5, //default is brightness
            recursion: {
                isUsed: false,
                depth: 4,
                grow: false,
                shrink: false,
                random: true
            },
            rotation: {
                isUsed: false,
                depth: 1,
            },
            name: 'Untitled',
            rulers: true,
            tolerance: 0.005,
            approximateWhite: false,
            approximateBlack: false,
            illuminant: 'D65',
            deltaE: '1976',
            observerAngle: 'TWO'
        };

        //set user options
        this.options = Vela.overwriteExisting(options, this.defaults);

        console.log(this.options);

        //ancillary nodes
        this.container = document.getElementById(this.selector);
        //this.console = document.getElementById('status');
        this.appBar = document.getElementById('application-bar');
        this.guts = document.getElementById('guts');
        this.controllers = document.querySelectorAll('.controller');

        //main canvas node and context
        this.canvas = document.getElementById('target');

        //compositing canvas node
        this.compositor = document.getElementById('compositor');

        //error message node
        this.errorWrapper = document.getElementById('error');

        //save master image pixels for rapid dissection
        //todo: i dont think i use this anymore since i refactored dissection
        //i dont use getDataUnderTile at all anymore
        //this.mPixels = [];

        //buckets for tiles
        this.masterTileBucket = [];
        this.globalTileBucket = [];
        this.assemblyTileBucket = [];

        //color formula constants covering 5 different illuminants
        //across 2 observer angles (2 and 10 degrees)
        //see http://www.easyrgb.com/en/math.php for full list (tristimulus reference values)
        //todo: these constants can probably be paired with their color space
        //and illuminant in a matrix. this would allow me to vectorize some of the math

        this.constants = {
            D50: {
                TWO: {
                    X: 96.422,
                    Y: 100,
                    Z: 82.521
                },
                TEN: {
                    X: 96.720,
                    Y: 100,
                    Z: 81.427
                }
            },
            D55: {
                TWO: {
                    X: 95.682,
                    Y: 100,
                    Z: 92.149
                },
                TEN: {
                    X: 95.799,
                    Y: 100,
                    Z: 90.926
                }
            },
            D65: {
                TWO: {
                    X: 95.047,
                    Y: 100,
                    Z: 108.883
                },
                TEN: {
                    X: 94.811,
                    Y: 100,
                    Z: 107.304
                }
            },
            D75: {
                TWO: {
                    X: 94.972,
                    Y: 100,
                    Z: 122.638
                },
                TEN: {
                    X: 94.416,
                    Y: 100,
                    Z: 120.641
                }
            },
            E: {
                TWO: {
                    X: 100,
                    Y: 100,
                    Z: 100
                },
                TEN: {
                    X: 100,
                    Y: 100,
                    Z: 100
                }
            }
        };

        //internal state management and random decor
        //this.status = '';
        this.quotes = [
            'a monument to all your sins',
            'two betrayals',
            'i feel the need. the need for speed'
        ];

        //stores the interface objects as a string ledger: 'controller[, id][, classes],listeners,actions'
        this.registry = [];

        //custom events for marking the editor as inprogress
        //todo: dont forget to update these event details. in fact, i may need to redo the whole state management
        this._inProgress = new CustomEvent('inprogress', { detail: 'cool' });
        this._notInProgress = new CustomEvent('notinprogress', { detail: 'neato' });

        this.init();
    }

    //----------
    //---------- static util methods, listed alphabetically
    //----------

    static eraseCookie(name) {
        Vela.setCookie(name, '', -1);
    }

    static getCookie(name) {
        let nameEQ = name + '=';
        let ca = document.cookie.split(';');

        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];

            while (c.charAt(0) == ' ') c = c.substring(1, c.length);

            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }

        return null;
    }

    //min is inclusive, max is exclusive: [min, max)
    static getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    //used to merge the users settings with the defaults
    static overwriteExisting(source, target) {
        let result = {};

        for (let key in target) {
            //todo: needs to be recursive or deep-checking if i continue to use nested objects
            //try this https://github.com/TehShrike/deepmerge/blob/master/index.js
            result[key] = (key in source && typeof source[key] === typeof target[key]) ? source[key] : target[key];
        }

        return result;
    }

    //http://www.jacklmoore.com/notes/rounding-in-javascript/
    static round(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }

    static setCookie(name, value, days) {
        //todo: 'let' with expires may not work here? needs testing
        if (days) {
            let date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            let expires = '; expires=' + date.toGMTString();
        } else {
            let expires = '';
        }

        document.cookie = name + '=' + value + expires + '; path=/';
    }

    static wrap(el, wrapper) {
        el.parentNode.insertBefore(wrapper, el);
        wrapper.appendChild(el);
    }

    //----------
    //---------- color manipulation algorithms, listed in logical order
    //---------- these all expect a single Tile as their argument and return the same tile
    //----------


    //todo: vectorize these. all operations with pixeldata should be vectorized
    //sums the red pixels in a tile and averages them
    red(t) {
        let d = t.map.data;
        let pool = 0;

        for (let i = 0; i < d.length; i += 4) {
            let y = d[i];

            pool += y;
        }

        t.avg = pool / (this.options.x * this.options.y);

        return t;
    }

    //sums the green pixels in a tile and averages them
    green(t) {
        let d = t.map.data;
        let pool = 0;

        for (let i = 0; i < d.length; i += 4) {
            let y = d[i + 1];

            pool += y;
        }

        t.avg = pool / (this.options.x * this.options.y);

        return t;
    }

    //sums the blue pixels in a tile and averages them
    blue(t) {
        let d = t.map.data;
        let pool = 0;

        for (let i = 0; i < d.length; i += 4) {
            let y = d[i + 2];

            pool += y;
        }

        t.avg = pool / (this.options.x * this.options.y);

        return t;
    }

    //todo: returns the average hue for a tile
    hue(t) {
        let d = t.map.data;
        let pool = 0;

        for (let i = 0; i < d.length; i += 4) {
            //let y = d[i];

            //pool += y;
        }

        t.avg = pool / (this.options.x * this.options.y);

        return t;
    }

    //todo: returns the average saturation for a tile
    saturation(t) {
        return t;
    }

    //returns the average brightness for a tile
    brightness(t) {
        //see https://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color

        let d = t.map.data;
        let pool = 0;

        for (let i = 0; i < d.length; i += 4) {
            //todo: implement different formulae and adjustments to these constants
            //see the lab converter i made, and add UI components like white point, temperature
            //perceptual luma, maybe chroma too
            let y = ((0.2126 * d[i]) + (0.7152 * d[i + 1]) + (0.0722 * d[i + 2]));

            //todo: add greyscale mode? assigning new values in here would yield different results
            //d[i] = y;
            //d[i + 1] = y;
            //d[i + 2] = y;

            pool += y;
        }

        //todo: i would rather have this statement use measurements from the incoming tile, like:
        //t.avg = pool / (d.width * d.height);
        t.avg = pool / (this.options.x * this.options.y);
        //t.avg = pool / (t.map.data.width * t.map.data.height);

        return t;
    }

    //returns the average RGB color of a tile. the tile's attribute
    //will be in the format [r, g, b]
    color(t) {
        let d = t.map.data;
        let pool = [0, 0, 0];

        for (let i = 0; i < d.length; i += 4) {
            pool[0] += d[i];
            pool[1] += d[i + 1];
            pool[2] += d[i + 2];
        }

        pool[0] /= (this.options.x * this.options.y);
        pool[1] /= (this.options.x * this.options.y);
        pool[2] /= (this.options.x * this.options.y);

        pool = this.rgbToLab(pool);

        t.avg = pool;

        return t;
    }

    //convert and rgb color to lab color for linear and perceptually uniform comparison
    //todo: this whole function can be vectorized
    //rgb comes in as an array and is returned as an array
    rgbToLab(rgb) {
        //normalize rgb components
        let r = rgb[0] / 255.0,
            g = rgb[1] / 255.0,
            b = rgb[2] / 255.0;

        //todo: figure out where these constants came from, too
        if (r > 0.04045) r = (((r + 0.055) / 1.055) ** 2.4);
        else r /= 12.92;

        if (g > 0.04045) g = (((g + 0.055) / 1.055) ** 2.4);
        else g /= 12.92;

        if (b > 0.04045) b = (((b + 0.055) / 1.055) ** 2.4);
        else b /= 12.92;

        r *= 100.0;
        g *= 100.0;
        b *= 100.0;

        //see http://brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
        //todo: vectorize e.g.: [x, y, z] = [r, g, b] * [constants_matrix]
        let x = r * 0.4124564 + g * 0.3575761 + b * 0.1804375;
        let y = r * 0.2126729 + g * 0.7151522 + b * 0.0721750;
        let z = r * 0.0193339 + g * 0.1191920 + b * 0.9503041;

        let C = this.constants[this.options.illuminant][this.options.observerAngle];

        x /= C.X;
        y /= C.Y;
        z /= C.Z;

        //todo: add to constants:
        //0.008856 is actually 216/24389
        //7.787 is actually 24389 / 27 / 116 or 24389 / 3132
        //see http://brucelindbloom.com/index.html?LabGamutDisplayHelp.html#IntegerLab

        if (x > 0.008856) x = (x ** 0.333333);
        else x = (7.787 * x) + (16.0 / 116.0);

        if (y > 0.008856) y = (y ** 0.333333);
        else y = (7.787 * y) + (16.0 / 116.0);

        if (z > 0.008856) z = (z ** 0.333333);
        else z = (7.787 * z) + (16.0 / 116.0);

        let CIE_L = (116.0 * y) - 16.0,
            CIE_a = 500.0 * (x - y),
            CIE_b = 200.0 * (y - z);

        return [CIE_L, CIE_a, CIE_b];
    }

    //calculate the linear perceptual distance using the CIE76 formula
    deltaE1976(lab1, lab2) {
        let L1 = lab1[0],
            a1 = lab1[1],
            b1 = lab1[2],
            L2 = lab2[0],
            a2 = lab2[1],
            b2 = lab2[2];

        return Math.sqrt(((L1 - L2) ** 2) + ((a1 - a2) ** 2) + ((b1 - b2) ** 2));
    }

    //calculate the linear perceptual distance using the CIE94 formula
    //slower to compute, yields totally different results than other formulae
    //used mainly for printed graphics and textiles, so apparently i missed the memo
    deltaE1994(lab1, lab2) {
        let L1 = lab1[0],
            a1 = lab1[1],
            b1 = lab1[2],
            L2 = lab2[0],
            a2 = lab2[1],
            b2 = lab2[2];

        let kL = 1,
            kC = 1,
            kH = 1;

        let xC1 = Math.sqrt(a1 ** 2 + b1 ** 2),
            xC2 = Math.sqrt(a2 ** 2 + b2 ** 2);

        let xDL = L2 - L1;
        let xDC = xC2 - xC1;

        let xDE = Math.sqrt(((L1 - L2) ** 2) + ((a1 - a2) ** 2) + ((b1 - b2) ** 2));

        let xDH = (xDE ** 2) - (xDL ** 2) - (xDC ** 2);

        if (xDH > 0) {
            xDH = Math.sqrt(xDH);
        } else {
            xDH = 0;
        }

        let xSC = 1 + (0.045 * xC1);
        let xSH = 1 + (0.015 * xC1);

        xDL /= kL;
        xDC /= kC * xSC;
        xDH /= kH * xSH;

        return Math.sqrt(xDL ** 2 + xDC ** 2 + xDH ** 2);
    }


    //----------
    //---------- exported methods, listed alphabetically
    //----------

    //close informational modal. this does not directly pertain to vela,
    //so i will probably move this to another file
    closeModal() {
        document.getElementById('modal-wrapper').classList.remove('visible');
        this.container.classList.remove('blur');
    }

    //https://stackoverflow.com/questions/36280818/how-to-convert-file-to-base64-in-javascript
    //expects fileobject, returns promise
    convertToBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    //expects img object, loads array with tile objects
    //used for all non-master images. use dissectMaster
    //if you need to populate masterTileBucket
    dissectImage(img) {
        if (img.name === this.options.master.name) {
            //weve encountered the master image, so skip it
            console.log(img.name + ' has been skipped');
            return false;
        }

        this.compositor.width = img.naturalWidth;
        this.compositor.height = img.naturalHeight;

        let ctx = this.compositor.getContext('2d');
        ctx.drawImage(img, 0, 0);

        let workingData = ctx.getImageData(0, 0, this.compositor.width, this.compositor.height);

        //overwrite alpha to be solid white
        if (this.isTransparent(workingData)) this.makeOpaque(workingData);

        //get tiles-per-width and tiles-per-height
        //todo: these need rounded
        let tilesPerWidth = Math.floor(img.naturalWidth / this.options.x);
        let tilesPerHeight = Math.floor(img.naturalHeight / this.options.y);

        //get the length of the loop
        let tileTotal = tilesPerWidth * tilesPerHeight;

        //for each tile, grab the corresponding pixels and calculate their
        //avg attribute, then store that in a tile object
        for (let i = 0; i < tileTotal; i++) {
            //initialize the variables for each tile
            let tempX = (i % tilesPerWidth) * this.options.x;
            let tempY = Math.floor(i / tilesPerWidth) * this.options.y;

            let localData = ctx.getImageData(tempX, tempY, this.options.x, this.options.y);

            //analyze each tile in this particular image
            //let tempAvg = this.getDataUnderTile(workingData, tempX, tempY);

            //note: i never use the index attr on tiles, but it helps for debugging
            //note: we are loading each tile with an avg of 0 here because later on we run filterbuckets
            //which assigns each tile an avg attribute based on the dissection mode
            this.globalTileBucket.push(new Tile(tempX, tempY, img.name, localData, 0, i));
        }
    }

    //this does not take a parameter as the master image is set on user
    //selection, and we can access it through the options object
    dissectMaster() {
        let ctx = this.canvas.getContext('2d');
        ctx.drawImage(this.options.master, 0, 0);

        //get tiles-per-width and tiles-per-height
        let tilesPerWidth = Math.floor(this.options.master.naturalWidth / this.options.x);
        let tilesPerHeight = Math.floor(this.options.master.naturalHeight / this.options.y);

        //get the length of the loop
        let tileTotal = tilesPerWidth * tilesPerHeight;

        for (let i = 0; i < tileTotal; i++) {
            //initialize the variables for each tile
            let tempX = (i % tilesPerWidth) * this.options.x;
            let tempY = Math.floor(i / tilesPerWidth) * this.options.y;

            let localData = ctx.getImageData(tempX, tempY, this.options.x, this.options.y);

            //let tempAvg = this.getDataUnderTile(this.mPixels, tempX, tempY);

            //note: i never use the index attr on tiles, but it helps for debugging
            //note: we are loading each tile with an avg of 0 here because later on we run filterbuckets
            //which assigns each tile an avg attribute based on the dissection mode
            this.masterTileBucket.push(new Tile(tempX, tempY, this.options.master.name, localData, 0, i));
        }
    }

    //dragstart, dragover, and drop are all related to moving the canvas around freely
    dragStart(e) {
        let style = window.getComputedStyle(e.target, null);
        let dims = (parseInt(style.getPropertyValue('left'), 10) - e.clientX) + ',' + (parseInt(style.getPropertyValue('top'), 10) - e.clientY);

        e.dataTransfer.setData("text/plain", dims);
    }

    dragOver(e) {
        e.preventDefault();
        return false;
    }

    drop(e) {
        let offset = e.dataTransfer.getData("text/plain").split(',');

        this.canvas.style.left = (e.clientX + parseInt(offset[0], 10)) + 'px';
        this.canvas.style.top = (e.clientY + parseInt(offset[1], 10)) + 'px';

        e.preventDefault();
        return false;
    }

    error(message) {
        //todo: this needs to work with multiple concurrent errors, like each error appends a new div
        let errorTimeout = 8000;
        this.errorWrapper.textContent = message;

        this.errorWrapper.style.display = 'block';

        setTimeout(() => {
            this.errorWrapper.style.display = 'none';
            this.errorWrapper.textContent = '';
        }, errorTimeout);
    }

    //perform pixel manipulation if specified
    //todo: vectorize this with matrix math
    filterBuckets(buckets) {
        for (let i = 0; i < buckets.length; i++) {
            for (let j = 0; j < buckets[i].length; j++) {
                let localTile = buckets[i][j];

                switch (this.options.mode) {
                    case 0: {
                        localTile = this.red(localTile);
                        break;
                    }
                    case 1: {
                        localTile = this.green(localTile);
                        break;
                    }
                    case 2: {
                        localTile = this.blue(localTile);
                        break;
                    }
                    case 3: {
                        localTile = this.hue(localTile);
                        break;
                    }
                    case 4: {
                        localTile = this.saturation(localTile);
                        break;
                    }
                    case 5: {
                        localTile = this.brightness(localTile);
                        break;
                    }
                    case 6: {
                        localTile = this.color(localTile);
                        break;
                    }
                    default: {
                        localTile = this.brightness(localTile);
                        break;
                    }
                }
            }
        }
    }

    findBestMatch(masterBucket, samplesBucket) {
        let bestDiff, currentDiff;

        if (this.options.mode === 6) {
            //cielab vars
            bestDiff = 255;
            currentDiff = 255;

            //here, a value of 2.3 represents a "just noticeable difference" (JND). perceptually,
            //any difference below this amount does not have a major impact on the result
            this.options.tolerance = 2.3;
        } else {
            //rgb vars
            bestDiff = 255;
            currentDiff = 255;
        }

        for (let i = 0; i < masterBucket.length; i++) {
            bestDiff = 255;

            for (let j = 0; j < samplesBucket.length; j++) {
                //measure the difference in avg of the sample tile to the master tile
                if (this.options.mode === 6) {
                    //if user is dissecting by color, use appropriate formula
                    if (this.options.deltaE === '1976') {
                        currentDiff = this.deltaE1976(masterBucket[i].avg, samplesBucket[j].avg);
                    } else if (this.options.deltaE === '1994') {
                        currentDiff = this.deltaE1994(masterBucket[i].avg, samplesBucket[j].avg);
                    } else {
                        throw new Error('Invalid Delta E formula selected');
                    }
                } else {
                    currentDiff = Math.abs(masterBucket[i].avg - samplesBucket[j].avg);
                }

                if (currentDiff <= bestDiff) {
                    //if the measured difference is smaller, we have a closer match
                    bestDiff = currentDiff;

                    this.assemblyTileBucket[i] = samplesBucket[j];

                    //todo: include an option here to filter out tiles that have been used.
                    //this would prevent repeats and could be an alternative to approximateWhite and approximateBlack

                    //break if tolerance is exceeded
                    if (bestDiff <= this.options.tolerance) {
                        break;
                    }
                }
            }
        }
    }

    //@deprecated: dissection saves all tile data to each tile
    //so using filterbuckets i can just load each tile with the proper attributes
    getDataUnderTile(imageData, startX, startY) {
        //figure out the coordinate for the tile relative to the image
        let endX = startX + this.options.x;
        let endY = startY + this.options.y;

        //init vars to measure/store the desired attribute
        let attributeAvg = 0;
        let attributePool = 0;

        //since the array is one-dimensional, we can just shift down the array to grab the desired attribute
        //ex: [r, g, b, a, r, g, b, a] = an entry for one pixel. to grab the "blue" component, shift up by 2
        //todo: this will likely need refactoring when i change how the mode is calculated
        let shift = 0;

        let data = imageData.data;

        //greyscale
        //note: make a copy and greyscale the copy. even better, make this a static method
        //for (let i = 0; i < pixelData.length; i += 4) {
        //    let greyAvg = (pixelData[i] + pixelData[i + 1] + pixelData[i + 2]) / 3;
        //    pixelData[i] = greyAvg;
        //    pixelData[i + 1] = greyAvg;
        //    pixelData[i + 2] = greyAvg;
        //}

        //walk across the tile and count the attribute
        for (let x = startX; x < endX; x++) {
            for (let y = startY; y < endY; y++) {
                //every entry of "pixel" here will always reference the RED component of the pixel at the index
                //attributePool += data[((y * (this.options.x * 4)) + (x * 4) + shift)];
                attributePool += data[(x + (y * imageData.width)) * 4];

                //todo: move mode switch block in here? that may hurt performance
            }
        }

        //divide the attribute pool by the total number of pixels in the tile
        attributeAvg = attributePool / (this.options.x * this.options.y);

        //run it back
        return attributeAvg;
    }

    hideLoader(e) {
        //console.log(e);
        //this.guts.classList.remove('inprogress');
        this.container.classList.remove('blur');
        document.getElementById('loader').classList.remove('visible');
    }

    init() {
        //any code in here only needs to runs once to set up the instance

        //empty registry if it exists (which would mean a reinit)
        this.registry.length = 0;

        //wrap frame in rulers div, which will be styled separately
        //todo: rewrite rulers as a function that actually draws rulers rather than loading an image
        if (this.showRulers) {
            let rulers = document.createElement('div');
            rulers.classList.add('rulers');
            Vela.wrap(this.frame, rulers);
        }

        //fill info with a random quote
        //todo: ill want to obscure this so that the quotes are server side
        document.getElementById('quote').textContent = this.quotes[Vela.getRandomInt(0, this.quotes.length)];

        //grab all controllers and attach listeners
        for (let controller of this.controllers) {
            //todo: make the separation character a variable
            let listeners = controller.dataset.listeners.split(',');
            let actions = controller.dataset.action.split(',');

            //todo: verify that events match against a list of valid events
            if (listeners.length !== actions.length) {
                //each listener must map to its own function
                console.log(controller + ' has mismatching listener/action pairs');
            } else if (listeners.length == actions.length) {
                //if the pairs match, walk through them and attach each respective action
                for (let i = 0; i < listeners.length; i++) {
                    if (typeof this[actions[i]] === 'function') {
                        //if its a valid function, listen for it
                        controller.addEventListener(listeners[i], (e) => {
                            this[actions[i]](e);

                            //todo: every single action goes through this hub
                            //i can assign a secondary validation function so that with each option changed,
                            //the new value can be written in a cookie to save preferences

                            //check event for useful properties
                            //console.log(e.currentTarget)
                        });

                        //if everything is good, register the controller
                        this.register(controller, listeners[i], actions[i]);
                    } else {
                        //highlight bad controllers
                        console.log(actions[i] + ' is not a function');
                        controller.classList.add('nosuchaction');
                    }
                }
            }
        }

        this.zoomer = new Zoom(this.guts, this.canvas, 4, 0.1, 0.25);
        this.fileHandler = new FileIO();

        this.guts.addEventListener('_inProgress', function () {
            //todo: educate myself on custom events. how the fuck do they work
            console.log('works');
        });

        //todo: all controls have defaults, those defaults need set here so that controllers inherit their correct properties
        //including setting a name to a cookie

        //alternatively, this could be one just by hard coding the values into the markup
        //although other one-time js operations still need to go here

        //console.log(this.registry);
    }

    //expects an array of pixel data
    isTransparent(data) {
        for (let i = 3; i < data.length; i += 4) {
            if (data[i] !== 255) {
                return true;
            }
        }

        return false;
    }

    //https://github.com/bfred-it/image-promise
    //expects a string or array of strings
    //generic promise-based loading function used by loadmaster and loadsamples
    load(image) {
        if (!image) {
            return Promise.reject();
        } else if (typeof image === 'string') {
            /* Create a <img> from a string */
            //console.log('incoming is a string');

            const src = image;
            image = new Image();
            image.src = src;
        } else if (image.length !== undefined) {
            //treat as multiple
            console.log('incoming is an array-like object');

            // Momentarily ignore errors
            const reflected = [].map.call(image, img => load(img).catch(err => err));

            return Promise.all(reflected).then(results => {
                const loaded = results.filter(x => x.naturalWidth);

                if (loaded.length === results.length) {
                    return loaded;
                }

                return Promise.reject({
                    loaded,
                    errored: results.filter(x => !x.naturalWidth)
                });
            });
        } else if (image.tagName !== 'IMG') {
            return Promise.reject();
        }

        const promise = new Promise((resolve, reject) => {
            if (image.naturalWidth) {
                // If the browser can determine the naturalWidth the
                // image is already loaded successfully
                console.log('resolved early using naturalwidth');

                resolve(image);
            } else if (image.complete) {
                // If the image is complete but the naturalWidth is 0px
                // it is probably broken
                console.log('rejected early using naturalwidth');

                reject(image);
            } else {
                //console.log('detecting naturalwidth...');

                image.addEventListener('load', fulfill);
                image.addEventListener('error', fulfill);
            }

            function fulfill() {
                if (image.naturalWidth) {
                    //console.log('we have naturalwidth! ', image.naturalWidth);
                    resolve(image);
                } else {
                    console.log('never resolved naturalwidth');
                    reject(image);
                }

                image.removeEventListener('load', fulfill);
                image.removeEventListener('error', fulfill);
            }
        });

        promise.image = image;
        return promise;
    }

    loadMaster(e) {
        //todo: make master image able to be resized
        this.guts.dispatchEvent(this._inProgress);

        //make sure user didnt cancel out of selection dialogue
        if (e.target.files && e.target.files.length > 0) {
            let fileObject = e.target.files[0];

            //validate the image before assignment
            if ((/\.(gif|jpg|jpeg|png)$/i).test(fileObject.name)) {

                //we are expecting a file object, so convert it to base64 and return that promise
                this.convertToBase64(fileObject).then(data => {

                    //load file using promises
                    this.load(data).then(img => {

                        //set a name for reference later. this is how the program knows to skip
                        //the master image if it is included as a sample
                        img.name = fileObject.name;

                        //store the returned image object
                        this.options.master = img;

                        //set canvas dimensions
                        this.canvas.width = this.options.master.naturalWidth;
                        this.canvas.height = this.options.master.naturalHeight;

                        //draw that mf thang
                        this.canvas.getContext('2d').drawImage(this.options.master, 0, 0);

                        //store the master img data so we dont have to later on
                        //this.mPixels = this.canvas.getContext('2d').getImageData(0, 0, this.options.master.naturalWidth, this.options.master.naturalHeight);
                    }).catch(function (err) {
                        console.log(err);
                    });
                }).catch(function (err) {
                    console.log(err);
                });
            } else {
                this.error('Master image is not a supported image type');
            }
        } else {
            //user cancelled out of dialogue
            this.guts.dispatchEvent(this._notInProgress);
        }

        this.guts.dispatchEvent(this._notInProgress);
    }

    //proxy function to call loadMaster after double-clicking on the canvas
    loadMasterTrigger() {
        document.getElementById('loadmaster').click();
    }

    loadSamples(e) {
        this.guts.dispatchEvent(this._inProgress);

        //essentially the same as loadMaster
        if (e.target.files && e.target.files.length > 0) {

            //clear old samples out
            //todo: in chrome, due to the directory attr on the button, if a user selects a folder of samples
            //but cancels out of the BROWSER APPROVAL DIALOGUE the samples array will be cleared anyway. this is
            //not desirable
            this.options.samples.length = 0;

            let fileObjects = e.target.files;

            //todo: clear webkitRelativePath for security?
            for (let i = 0; i < fileObjects.length; i++) {
                let currentFile = fileObjects[i];

                //validate the image before assignment
                if ((/\.(gif|jpg|jpeg|png)$/i).test(currentFile.name)) {

                    //we are expecting a file object, so convert it to base64 and return that promise
                    this.convertToBase64(currentFile).then(data => {

                        //load file using promises
                        this.load(data).then(img => {

                            //assign name and other properties to each img here
                            //this could be very powerful as i am creating a fresh img object
                            //so i can tack on whatever properties i want like tileCount
                            img.name = currentFile.name;

                            //push the img to the samples stack
                            this.options.samples.push(img);
                        }).catch(function (err) {
                            console.log(err);
                        });
                    }).catch(function (err) {
                        console.log(err);
                    });
                } else {
                    console.log(currentFile.name + ' is not a supported image type');
                }
            }
        } else {
            //user cancelled out of dialogue
            this.guts.dispatchEvent(this._notInProgress);
        }

        this.guts.dispatchEvent(this._notInProgress);
    }

    loadSamplesFromUnsplash(e) {
        //placeholder
    }

    makeOpaque(data) {
        for (let i = 3; i < data.length; i += 4) {
            data[i] = 255;
        }

        //todo: figure out if i need a return here
        return data;
    }

    //purposeful function to cancel events and return nothing
    //the console log is so users can see when "nothing" happens on purpose
    //as opposed to accidentally. this way, even when "nothing" happens, something happens
    noop(e) {
        console.log(`noop intentionally called from ${e.target.tagName}${(e.target.id ? '#' + e.target.id : '')}${(e.target.classList.value ? '.' + e.target.classList.value : '')}`);
        //e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        return false;
    }

    //put the result back together
    reconstruct() {
        //calculate new dimensions for the remade image
        let tilesPerWidth = Math.floor(this.options.master.naturalWidth / this.options.x);

        let newWidth = this.options.master.naturalWidth - (this.options.master.naturalWidth % this.options.x);
        let newHeight = this.options.master.naturalHeight - (this.options.master.naturalHeight % this.options.y);

        //set size of new canvases
        //todo: this will gradually eat away at the image dimensions over consecutive passes
        //instead, maybe subtract from the master image naturalwidth/naturalheight?
        this.compositor.width = newWidth;
        this.compositor.height = newHeight;
        this.canvas.width = newWidth;
        this.canvas.height = newHeight;


        if (this.assemblyTileBucket.length !== this.masterTileBucket.length) {
            //console.log('there must have been an error building the assembly bucket. assembly and master bucket lengths do not match');
            throw new Error('there must have been an error building the assembly bucket. assembly and master bucket lengths do not match');
        }

        //save reference to context for later
        let ctx = this.canvas.getContext('2d');

        //loop through assembly bucket and put it all together
        for (let i = 0; i < this.assemblyTileBucket.length; i++) {
            let xWalker = (i % tilesPerWidth) * this.options.x;
            let yWalker = Math.floor(i / tilesPerWidth) * this.options.y;

            //todo: add approximatewhite/black flags in here and expose thresholds as settable properties
            if (this.options.approximateBlack && this.assemblyTileBucket[i].avg < 1.0) {

            } else if (this.options.approximateWhite && this.assemblyTileBucket[i].avg > 254.0) {

            } else {
                //console.log(this.assemblyTileBucket[i].map);
                ctx.putImageData(this.assemblyTileBucket[i].map, xWalker, yWalker);
            }
        }

        //todo: this bloats the entire options object. maybe store the input in a hidden input somewhere?
        this.options.result.src = this.compositor.toDataURL();
    }

    //keep track of the full controller interface in this.registry
    register(controller, listener, action) {
        let entry = controller.tagName;

        if (controller.id) entry += '#' + controller.id;

        for (let i = 0; i < controller.classList.length; i++) {
            entry += '.' + controller.classList[i];
        }

        entry += ';' + listener + ';';
        entry += action;

        this.registry.push(entry);
    }

    resetCanvasCoordinates(e) {
        //reset inline styles from zoomer
        this.canvas.style.transform = 'translate(0, 0) scale(1, 1)';

        //reset inline styles from dragging
        this.canvas.style.top = '0';
        this.canvas.style.left = '0';

        //reset readout
        e.target.textContent = '100%';
    }

    save() {
        let savedImage = this.canvas.toDataURL("image/png");//.replace("image/png", "image/octet-stream");
        //window.location.href = savedImage;

        let link = document.createElement('a');
        link.download = this.options.name;
        link.href = savedImage;
        link.click();

        document.getElementById('name').classList.add('saved');
    }

    setObserverAngle(e) {
        this.options.observerAngle = e.target.value;

        //console.log(this.constants[this.options.illuminant][this.options.observerAngle]);
    }

    setBounds(e) {
        this.options[e.target.id] = parseInt(e.target.value, 10);
    }

    setColorAlgorithm(e) {
        this.options.deltaE = e.target.value.split(' ')[1];
    }

    setIlluminant(e) {
        this.options.illuminant = e.target.value;
    }

    setMode(e) {
        //selects the index for the algorithm the user wants to use
        this.options.mode = e.target.selectedIndex;
    }

    setName(e) {
        let forbiddenChars = (/[\:\;\"\'\<\>\,\%\\\/\[\]\{\}\(\)\*\?\|\^]/g.test(e.target.textContent)); // forbidden characters \ / : * ? " < > |
        let leadingDot = /^\./.test(e.target.textContent); // cannot start with dot (.)

        //prevent space and enter from firing at all
        if (e.keyCode === 32 || e.code === 'Space') {
            e.preventDefault();
        }

        //catch enter specifically, and treat it as 'submitting' the name
        if (e.keyCode === 13 || e.code === 'Enter') {
            e.target.blur();
        }

        //if its a blur event or enter key, set the name option
        //todo: make the enter key perform the same function
        if (e.type === 'blur' || e.keyCode === 13 || e.code === 'Enter') {
            //e.preventDefault();

            if (forbiddenChars) {
                e.target.textContent = e.target.textContent.replace(/[\:\;\"\'\<\>\,\%\\\/\[\]\{\}\(\)\*\?\|\^]/g, '');
                this.error('name cannot contain the characters :;\"\'<>,%\/[]{}()*?|^');
            } else if (leadingDot) {
                e.target.textContent = e.target.textContent.replace(/^\./, '');
                this.error('name connot start with a period (.)');
            } else {
                this.options.name = e.target.textContent;
                e.target.classList.add('saved');
            }
        }
    }

    setRecursion(e) {
        this.options.recursion.isUsed = e.target.checked;

        document.getElementById('recursiondepth').disabled = !e.target.checked;
    }

    setRecursionDepth(e) {
        this.options.recursion.depth = e.target.value;
    }

    setStatus(i) {
        let states = [
            'mint',
            'initialized',
            'working',
            'no-op',
            'finished'
        ];

        if (i > states.length) {
            console.log('not a valid state');
            return false;
        }

        console.log(i);
        this.status = states[i];
    }

    setTheme(e) {
        //todo: this can be written better
        this.container.classList.remove('light', 'dark');
        this.container.classList.add(e.target.value);
    }

    showLoader(e) {
        //console.log(e);

        //this.guts.classList.add('inprogress');
        this.container.classList.add('blur');
        document.getElementById('loader').classList.add('visible');
    }

    showModal() {
        document.getElementById('modal-wrapper').classList.add('visible');
        this.container.classList.add('blur');
        document.getElementById('modal').focus();
        //todo: if a user edits the options object, those edits will persist across opening the modal multiple times
        document.getElementById('obj').textContent = JSON.stringify(this.options, null, 4);
    }

    swapDimensions() {
        //width becomes height, height becomes width
        let temp = this.options.y;
        this.options.y = this.options.x;
        this.options.x = temp;

        document.getElementById('x').value = this.options.x;
        document.getElementById('y').value = this.options.y;
    }

    testEvent(e) {
        this.error(e.type + ' works');
    }

    toggleAppBar(e) {
        this.appBar.classList.toggle('collapsed');
    }

    vela(e) {
        //todo: should probably be an ajax call sending options object to the server
        //todo: should probably disable 'run' button in between requests too

        //leave uncommented unless testing writing data to file on server
        e.preventDefault();

        //run checks to make sure we can run a dissection
        if (this.options.master == null) {
            this.error('you must select a valid master image first');
            return false;
        } else if (this.options.samples.length <= 0) {
            this.error('you must select one or more sample images first');
            return false;
        }

        //mark in progress
        this.guts.dispatchEvent(this._inProgress);

        setTimeout(() => {
            //start timer
            let now = Date.now();

            //todo: add timer checkpoints/benchmarks after each step
            //might as well add in a full debug object too? then i can write a gulp task to build out vela minus the debugger

            //empty all buckets
            //todo: check for references to these arrays elsewhere. references after
            //this has been emptied cause the backing array not to be garbage collected
            //and these objects are fucking massive
            this.globalTileBucket.length = 0;
            this.masterTileBucket.length = 0;
            this.assemblyTileBucket.length = 0;

            //dissect master first
            this.dissectMaster();

            //dissect each sample
            for (let i = 0; i < this.options.samples.length; i++) {
                this.dissectImage(this.options.samples[i]);
            }

            //filter buckets here instead of when collecting data
            this.filterBuckets([this.masterTileBucket, this.globalTileBucket]);

            //populate assembly bucket
            this.findBestMatch(this.masterTileBucket, this.globalTileBucket);

            this.reconstruct();

            //cacheOldMasterImage();
            //displayNewMasterImage();

            //log timer
            //todo: record and write data to json file. im fine with using a dependency to do this
            let later = Date.now();
            console.log(later - now);

            this.canvas.getContext('2d').drawImage(this.options.result, 0, 0);

            this.guts.dispatchEvent(this._notInProgress);
        }, 10);

        //dont put anything after the timeout related to painting/styling

        //i clear the buckets here to save memory. not sure yet if its working
        this.globalTileBucket.length = 0;
        this.masterTileBucket.length = 0;
        this.assemblyTileBucket.length = 0;
    }

    zoom(e) {
        //enable zoom only while ctrl key is down
        if (e.ctrlKey) {
            this.zoomer.scrolled(e);
        } else {
            return false;
        }
    }
}
