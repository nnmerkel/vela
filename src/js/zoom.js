//helper class for zooming on scroll
//https://stackoverflow.com/questions/46647138/zoom-in-on-a-mousewheel-point-using-scale-and-translate

class Zoom {
    //target is the element needing to be zoomed
    //container is the parent of target
    //maxscale is how many times larger target will grow in the XY plane before it stops zooming
    //minscale is same but lower bound
    //zoomfactor is incrementally how much users zoom per wheel event

    constructor(container, target, maxScale, minScale, zoomFactor) {
        this.container = container;
        this.maxScale = maxScale;
        this.minScale = minScale;
        this.zoomFactor = zoomFactor;

        this.target = target;
        this.mag = document.getElementById('magnification');
        this.size = { w: this.target.offsetWidth, h: this.target.offsetHeight };
        this.pos = { x: 0, y: 0 };
        this.zoomTarget = { x: 0, y: 0 };
        this.zoomPoint = { x: 0, y: 0 };
        this.scale = 1;

        target.style.transformOrigin = '0 0';
    }

    scrolled(e) {
        e.preventDefault();

        let offset = this.container.getBoundingClientRect();

        this.zoomPoint.x = e.pageX - offset.left;
        this.zoomPoint.y = e.pageY - offset.top;

        //incoming delta is either -100 or 100, so right shift and default to 1
        let delta = -1 * (((e.deltaY || -e.wheelDelta || e.detail) >> 10) || 1);

        if (delta > 1 || delta < -1) console.log('you suck');
        //delta will only ever be 1 or -1, but if you ever see the above message, uncomment line below
        //delta = Math.max(-1, Math.min(1, delta)); // cap the delta to [-1,1] for cross browser consistency

        // determine the point on where the slide is zoomed in
        this.zoomTarget.x = (this.zoomPoint.x - this.pos.x) / this.scale;
        this.zoomTarget.y = (this.zoomPoint.y - this.pos.y) / this.scale;

        // apply zoom
        this.scale += delta * this.zoomFactor * this.scale;
        this.scale = Math.max(this.minScale, Math.min(this.maxScale, this.scale));

        // calculate x and y based on zoom
        this.pos.x = -this.zoomTarget.x * this.scale + this.zoomPoint.x;
        this.pos.y = -this.zoomTarget.y * this.scale + this.zoomPoint.y;

        // Make sure the slide stays in its container area when zooming out
        if (this.pos.x > 0)
            this.pos.x = 0;
        if (this.pos.x + this.size.w * this.scale < this.size.w)
            this.pos.x = -this.size.w * (this.scale - 1);
        if (this.pos.y > 0)
            this.pos.y = 0;
        if (this.pos.y + this.size.h * this.scale < this.size.h)
            this.pos.y = -this.size.h * (this.scale - 1);

        //this.target.style.transform = 'translate(' + (this.pos.x) + 'px,' + (this.pos.y) + 'px) scale(' + this.scale + ',' + this.scale + ')';
        this.target.style.transform = `translate(${this.pos.x}px, ${this.pos.y}px) scale(${this.scale}, ${this.scale})`;

        //this.mag.textContent = `${Vela.round(this.scale * 100, 3)}%`;
        this.mag.textContent = this.scale * 100;
    }
}
